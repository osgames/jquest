/**
 * 	Created on Mar 18, 2006
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * 	@author Rob Haden
 */
public class Sector {
	
	public static final int SIDE_LENGTH = 28;
	public static final int MINI_LENGTH = 12;
	
	private static final Color DARK_GREEN = new Color(0, 0.4f, 0);

	private Color color;
	private Planet planet;
	private int row, column;
	
	/** Constructor */
	public Sector(int row, int column) {
		this.row = row;
		this.column = column;
		removePlanet();
	}
	
	/** Creates a planet for the sector with the specified owner */
	public void createPlanet(Player player) {
		if(player == null) {
			planet = Planet.createNeutralPlanet(this);
			color = Color.LIGHT_GRAY;
		} else {
			planet = Planet.createPlayerPlanet(this, player);
			color = player.getColor();
		}
	}
	
	/** Removes a planet from the sector */
	public void removePlanet() {
		planet = null;
		color = DARK_GREEN;
	}
	
	/** Returns whether the sector has a planet */
	public boolean hasPlanet() {
		return (planet != null);
	}
	
	/** Draws the sector */
	public void draw(Graphics g, int tileX, int tileY) {
		int pixelX = tileX * SIDE_LENGTH;
		int pixelY = tileY * SIDE_LENGTH;
		g.setColor(color);
		g.drawRect(pixelX, pixelY, SIDE_LENGTH - 1, SIDE_LENGTH - 1);
		if(hasPlanet()) planet.draw(g, pixelX, pixelY);
	}
	
	/** Draws the sector as highlighted */
	public void drawHighlight(Graphics g, int tileX, int tileY) {
		if(!hasPlanet()) return;
		int pixelX = tileX * SIDE_LENGTH;
		int pixelY = tileY * SIDE_LENGTH;
		g.setColor(Color.WHITE);
		g.drawRect(pixelX, pixelY, SIDE_LENGTH - 1, SIDE_LENGTH - 1);
	}
	
	/** Draws the sector on a mini-map */
	public void drawMini(Graphics g, int tileX, int tileY) {
		if(!hasPlanet()) return;
		int pixelX = tileX * MINI_LENGTH;
		int pixelY = tileY * MINI_LENGTH;
		g.setColor(color);
		g.fillOval(pixelX, pixelY, MINI_LENGTH, MINI_LENGTH);
	}
	
	/** Updates the sector's planet, if it has one */
	public void updatePlanet() {
		if(hasPlanet()) planet.update();
	}
	
	/** Returns the sector's planet, if it has one */
	public Planet getPlanet() {
		return planet;
	}
	
	/** Returns a string containing the planet information */
	public String getPlanetInfo() {
		if(hasPlanet()) {
			return planet.getInfoString();
		}
		return Planet.NULL_INFO;
	}
	
	/** Returns the sector's location as a Point */
	public Point getLocation() {
		return new Point(row, column);
	}
	
	/** Changes the color of this sector */
	public void updateColor() {
		color = planet.getOwner().getColor();
	}
	
}
