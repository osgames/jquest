/**
 * 	Created on Mar 25, 2006
 */

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

/**
 * 	@author Rob Haden
 */
public class PlanetInfo extends JTextArea implements Observer {
	
	private static final int WIDTH = 152;
	private static final int HEIGHT = 100;

	public PlanetInfo() {
		super();
		setForeground(Color.WHITE);
		setOpaque(false);
		setText(Planet.NULL_INFO);
		setEditable(false);
		setAlignmentX(Component.LEFT_ALIGNMENT);
		setMaximumSize(new Dimension(
			WIDTH, HEIGHT));
	}

	public void update(Observable o, Object arg) {
		if(!(arg instanceof String)) return;
		setText((String)arg);
	}
	
}
