/**
 * 	Created on Mar 18, 2006
 */

import static java.lang.Math.random;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

/**
 * 	@author Rob Haden
 */
public class Planet {
	
	public static final int NUM_PLANET_TYPES = 8;
	public static final String NULL_INFO = 
		"Name: \nOwner: \nShips: \nProduction: \nKill Percent: ";
	
	private static final int PRODUCTION_RANGE = 10;
	private static final int MIN_PRODUCTION = 5;
	private static final float KILL_PERCENT_RANGE = 0.7f;
	private static final float MIN_KILL_PERCENT = 0.3f;
	
	private static String names =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*(),.<>;:[]{}/?-+\\|";
		
	public static int nameIndex = 0;

	private Image image;
	private Sector sector;
	private String name;
	private Player owner;
	private Fleet homeFleet;
	private int production;
	private float killPercent;
	
	/** Creates and returns a player's home planet */
	public static Planet createPlayerPlanet(Sector sector, Player owner) {
		return new Planet(sector, owner, 10, 0.4f);
	}
	
	/** Creates and returns a new random planet */
	public static Planet createNeutralPlanet(Sector sector) {
		return new Planet(sector, null,
				(int)(random() * PRODUCTION_RANGE) + MIN_PRODUCTION,
				(float)((random() * KILL_PERCENT_RANGE) + MIN_KILL_PERCENT)
		);
	}
	
	/** Constructor */
	private Planet(Sector sector, Player owner,
				  int production, float killPercent) {
		name = names.substring(nameIndex, ++nameIndex);
		this.sector = sector;
		this.owner = owner;
		this.production = production;
		this.killPercent = killPercent;
		int planetType = (int)(random() * (NUM_PLANET_TYPES - 1)) + 1;
		image = JQuest.PLANET_IMAGES[planetType];
		homeFleet = new Fleet(owner, (isNeutral() ?
				(production - 1) : 0), killPercent);
	}
	
	/** Returns whether the planet is owned */
	public boolean isNeutral() {
		return (owner == null);
	}
	
	/** Returns whether the planet is owned by the given player */
	public boolean hasOwner(Player p) {
		if(isNeutral()) return false;
		return owner.equals(p);
	}
	
	/** Updates the planet */
	public void update() {
		if(!isNeutral()) {
			homeFleet.addShips(production);
			owner.updateShipsBuilt(production);
		} else {
			homeFleet.addShips(1);
		}
		// homeFleet.addShips(isNeutral() ? 1 : production);
	}
	
	/** The planet falls to an invading fleet */
	public void conquered(Fleet invader) {
		owner = invader.getOwner();
		homeFleet = invader;
		sector.updateColor();
	}
	
	/** Draws the planet's image */
	public void draw(Graphics g, int x, int y) {
		g.drawImage(image, x, y, null);
		g.setColor(Color.WHITE);
		g.drawString(name, x + Sector.SIDE_LENGTH - 10,
				y + Sector.SIDE_LENGTH - 2);
	}
	
	/** Displays the planet's info */
	public String getInfoString() {
		return "Name: " + name +
			   "\nOwner: " + (isNeutral() ? "" : owner) +
			   "\nShips: " + homeFleet.getShipCountString() +
			   "\nProduction: " + production +
			   "\nKill Percent: " + killPercent;
	}
	
	/** Resets the name index */
	public static void resetNameIndex() {
		nameIndex = 0;
	}
	
	/** Returns the distance to another planet */
	public float getDistance(Planet other) {
		return (float)sector.getLocation().distance(
				other.getSector().getLocation());
	}
	
	/** Returns the planet's owner */
	public Player getOwner() {
		return owner;
	}
	
	/** Returns the planet's sector */
	public Sector getSector() {
		return sector;
	}
	
	/** Returns the planet's home fleet */
	public Fleet getHomeFleet() {
		return homeFleet;
	}
	
	/** Returns the planet's name -- overrides toString() */
	public String toString() {
		return name;
	}
	
}
