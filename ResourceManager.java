/**
 * 	Created on Mar 17, 2006
 */

import java.awt.Image;
import java.util.*;
import javax.swing.ImageIcon;

/**
 * 	@author Rob Haden
 */
public class ResourceManager {
	
	private HashMap<String, Image> images;
	
	/** Constructor */
	public ResourceManager() {
		images = new HashMap<String, Image>();
	}
	
	/** Loads an image and returns it */
	public Image loadImage(String name) {
		// If already loaded, find it and return it
		if(images.containsKey(name)) {
			return images.get(name);
		}
		
		// Otherwise, load it and then return it
		Image image = new ImageIcon("images/" + name).getImage();
		images.put(name, image);
		return image;
	}
	
}
