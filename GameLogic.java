/**
 * 	Created on Mar 20, 2006
 */

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * 	@author Rob Haden
 */
public class GameLogic extends Observable
					   implements ActionListener, KeyListener, Observer {
	
	public static enum GameState {
		NONE, SOURCE_PLANET, DEST_PLANET, SHIP_COUNT,
		RULER_SOURCE, RULER_DEST, AI_PLAYER
	}
	
	private static final String SHIP_COUNT_ERROR =
		"Not enough ships to send.";
	private static final String PLANET_HOLDS =
		" has held against an attack from ";
	private static final String PLANET_FALLEN =
		" has fallen to ";
	private static final String REINFORCEMENTS =
		"Reinforcements have arrived for Planet ";

	private GameState state;
	private List<Fleet> attackFleets;
	private List<Player> players;
	private Map map;
	private Planet sourcePlanet, destPlanet;
	private Player currentPlayer;
	private int turnCount, maxTurns, shipCount;
	
	/** Constructor */
	public GameLogic(List<Player> players, Map map) {
		this.players = players;
		this.map = map;
		this.attackFleets = new ArrayList<Fleet>();
		map.addObserver(this);
	}
	
	/** Cleans up the game data */
	public void cleanUpGame() {
		attackFleets.clear();
		map.clear();
		players.clear();
		sourcePlanet = null;
		destPlanet = null;
		currentPlayer = null;
	}
	
	/** Changes the game state */
	public void changeState(GameState newState) {
		if(state == newState) return;
		state = newState;
		setChanged();
		notifyObservers(state);
	}

	/** Updates the game logic based on game input */
	public void update(Observable o, Object arg) {
		if((o instanceof NewGameDialog) && (arg instanceof Integer)) {
			turnCount = 0;
			maxTurns = ((Integer)arg).intValue();
			nextTurn();
		} else if((o instanceof Map) && (arg instanceof Planet)) {
			Planet target = (Planet)arg;
			switch(state) {
			case SOURCE_PLANET:
				if(target.hasOwner(currentPlayer)) {
					sourcePlanet = target;
					processTurn();
				}
				break;
			case RULER_SOURCE:
				sourcePlanet = target;
				processTurn();
				break;
			case DEST_PLANET:
			case RULER_DEST:
				destPlanet = target;
				processTurn();
				break;
			default:
				break;
			}
		}
	}
	
	/** Receives events from Swing components */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
			nextPlayer();
		} else if(e.getSource() instanceof JTextField) {
			getShipCount((JTextField)e.getSource());
		}
	}
	
	/** Gets the number of ships to send */
	public void getShipCount(JTextField field) {
		if(state != GameState.SHIP_COUNT) return;
		try {
			shipCount = Integer.parseInt(field.getText());
			int distance = (int)sourcePlanet.getDistance(destPlanet);
			Fleet fleet = sourcePlanet.getHomeFleet().spawnFleet(
					shipCount, destPlanet, distance);
			sourcePlanet = null;
			destPlanet = null;
			if(fleet == null) {
				setChanged();
				notifyObservers(SHIP_COUNT_ERROR);
			} else {
				attackFleets.add(fleet);
				changeState(GameState.SOURCE_PLANET);
			}
		} catch (NumberFormatException e) {}
	}
	
	/** Processes the turn for the current player */
	private void processTurn() {
		switch(state) {
		case SOURCE_PLANET:
			if(sourcePlanet != null) changeState(GameState.DEST_PLANET);
			break;
		case DEST_PLANET:
			if(destPlanet != null) changeState(GameState.SHIP_COUNT);
			break;
		case RULER_SOURCE:
			if(sourcePlanet != null) changeState(GameState.RULER_DEST);
			break;
		case RULER_DEST:
			if(destPlanet != null) {
				float distance = sourcePlanet.getDistance(destPlanet);
				setChanged();
				notifyObservers("The distance from " + sourcePlanet +
						" to " + destPlanet + " is " + distance +
						" light years.\nA ship leaving this turn" +
						" will arrive on turn " +
						(int)(turnCount + distance));
				sourcePlanet = null;
				destPlanet = null;
				changeState(GameState.SOURCE_PLANET);
			}
			break;
		case AI_PLAYER:
			List<Planet> planets = map.getPlanets();
			Planet target = null;
			for(Planet p : planets) {
				if(!p.hasOwner(currentPlayer) ||
						(p.getHomeFleet().getAttackStrength() < 20)) {
					continue;
				}
				if((target = currentPlayer.searchForPlanet(
						planets, p, true)) != null) {
					attackFleets.add(p.getHomeFleet().spawnFleet(
							p.getHomeFleet().getAttackStrength(),
							target, (int)p.getDistance(target)));
				} else if((target = currentPlayer.searchForPlanet(
						planets, p, false)) != null) {
					attackFleets.add(p.getHomeFleet().spawnFleet(
							p.getHomeFleet().compareSize(
									target.getHomeFleet()) / 2,
							target, (int)p.getDistance(target)));
				}
			}
			nextPlayer();
			break;
		case NONE:
		default:
			break;
		}
	}
	
	/** Updates the game logic */
	private void nextTurn() {
		updateAttackFleets();
		scanForSurvivors();
		turnCount++;
		map.update();
		Player winner = findWinner();
		if(winner != null) {
			JOptionPane.showMessageDialog(null, "The mighty " + winner +
					" has conquered the galaxy!", "Game Over",
					JOptionPane.INFORMATION_MESSAGE);
			changeState(GameState.NONE);
		} else if(turnCount >= maxTurns) {
			JOptionPane.showMessageDialog(null,
					"The maximum number of turns has been reached.\n" +
					"This game is now over.",
					"Game Over", JOptionPane.INFORMATION_MESSAGE);
			changeState(GameState.NONE);
		} else {
			setChanged();
			notifyObservers(new Integer(turnCount));
			setCurrentPlayer(players.get(0));
		}
	}
	
	/** Starts the next player's turn */
	private void nextPlayer() {
		int playerIndex = players.indexOf(currentPlayer) + 1;
		if(playerIndex >= players.size()) {
			nextTurn();
		} else {
			setCurrentPlayer(players.get(playerIndex));
		}
	}
	
	/** Sets the current player */
	private void setCurrentPlayer(Player p) {
		currentPlayer = p;
		setChanged();
		notifyObservers(currentPlayer);
		if(currentPlayer.isAI()) {
			changeState(GameState.AI_PLAYER);
			processTurn();
		} else {
			changeState(GameState.SOURCE_PLANET);
		}
	}
	
	/** Updates the currently existing attack fleets */
	private void updateAttackFleets() {
		List<Fleet> removeList = new ArrayList<Fleet>();
		for(Fleet fleet : attackFleets) {
			fleet.update();
			if(fleet.hasArrived()) {
				resolveCombat(fleet);
				removeList.add(fleet);
			}
		}
		attackFleets.removeAll(removeList);
	}
	
	/** Resolves the combat for an arriving fleet */
	private void resolveCombat(Fleet arrivingFleet) {
		Fleet defenseFleet = arrivingFleet.getDest().getHomeFleet();
		if(arrivingFleet.isFriendly()) {
			arrivingFleet.mergeWith(defenseFleet);
			setChanged();
			notifyObservers(REINFORCEMENTS + arrivingFleet.getDest());
		} else {
			while(!arrivingFleet.isEmpty() && !defenseFleet.isEmpty()) {
				if(defenseFleet.attacks()) {
					arrivingFleet.removeShips(1);
					if(defenseFleet.getOwner() != null) {
						defenseFleet.getOwner().updateShipsDestroyed(1);
					}
				}
				if(arrivingFleet.attacks()) {
					defenseFleet.removeShips(1);
					arrivingFleet.getOwner().updateShipsDestroyed(1);
				}
			}
			if(arrivingFleet.isEmpty()) {
				// Planet holds
				setChanged();
				notifyObservers("Planet " + arrivingFleet.getDest() +
						PLANET_HOLDS + arrivingFleet.getOwner());
				if(defenseFleet.getOwner() != null) {
					defenseFleet.getOwner().updateFleetsDestroyed(1);
				}
			} else if(defenseFleet.isEmpty()) {
				// Planet conquered
				setChanged();
				notifyObservers("Planet " + arrivingFleet.getDest() +
						PLANET_FALLEN + arrivingFleet.getOwner());
				arrivingFleet.conquerPlanet();
				arrivingFleet.getOwner().updateFleetsDestroyed(1);
			}
		}
	}
	
	/** Scans the galaxy for surviving players */
	private void scanForSurvivors() {
		List<Player> inactivePlayers = new ArrayList<Player>(players);
		for(Planet p : map.getPlanets()) {
			if(!p.isNeutral()) {
				inactivePlayers.remove(p.getOwner());
			}
		}
		for(Fleet f : attackFleets) {
			inactivePlayers.remove(f.getOwner());
		}
		for(Player p : inactivePlayers) {
			JOptionPane.showMessageDialog(null, "The once mighty empire of " +
					p + " has fallen in ruins.");
			players.remove(p);
		}
	}
	
	/** Finds the winner of the game, if there is one yet */
	private Player findWinner() {
		Player winner = null;
		int activePlayers = 0;
		for(Player p : players) {
			winner = p;
			activePlayers++;
		}
		if(activePlayers == 1) {
			return winner;
		}
		return null;
	}
	
	/** Returns to the previous state */
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			switch(state) {
			case DEST_PLANET:
			case SHIP_COUNT:
			case RULER_SOURCE:
			case RULER_DEST:
				changeState(GameState.SOURCE_PLANET);
				sourcePlanet = null;
				destPlanet = null;
				break;
			default: break;
			}
		}
	}

	/** From the KeyListener interface */
	public void keyTyped(KeyEvent e) {
		// do nothing
	}

	/** From the KeyListener interface */
	public void keyReleased(KeyEvent e) {
		// do nothing
	}
	
}
