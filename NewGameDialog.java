/**
 * 	Created on Mar 19, 2006
 */

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;
import javax.swing.event.*;

/**
 * 	@author Rob Haden
 */
public class NewGameDialog extends Observable
						   implements ActionListener {
	
	private static final int WIDTH = 610;
	private static final int HEIGHT = 435;
	private static final int MIN_PLAYERS = 2;
	private static final int MAX_PLAYERS = 9;
	private static final int DEFAULT_PLAYERS = 2;
	private static final int MIN_PLANETS = 1;
	private static final int MAX_PLANETS = 35;
	private static final int DEFAULT_PLANETS = 3;
	private static final int MIN_TURNS = 5;
	private static final int MAX_TURNS = 40;
	private static final int DEFAULT_TURNS = 15;

	private JDialog window;
	private JButton addButton, okButton, cancelButton,
					defaultsButton, rejectButton;
	private JLabel playerLabel, planetLabel, turnLabel;
	private JList playerList;
	private JSlider playerSlider, planetSlider, turnSlider;
	private JTextField playerNameField;
	private List<Player> players;
	private Map map;
	private DefaultListModel playerListModel;
	private boolean startGame;
	
	/** Constructor */
	public NewGameDialog(JFrame frame, Map map, List<Player> players) {
		// Set up member variables from constuctor parameters
		this.map = map;
		this.players = players;
		
		// Set up labels and player list
		playerLabel = new JLabel();
		planetLabel = new JLabel();
		planetLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		turnLabel = new JLabel();
		playerListModel = new DefaultListModel();
		playerList = new JList(playerListModel);
		playerList.setLayoutOrientation(JList.VERTICAL);
		// playerList.setMaximumSize(new Dimension(270, 166));
		// playerList.setVisibleRowCount(-1);
		playerNameField = new JTextField(12);
		
		// Set up sliders
		playerSlider = new JSlider(JSlider.HORIZONTAL,
				MIN_PLAYERS, MAX_PLAYERS, DEFAULT_PLAYERS);
		playerSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(playerSlider.getValueIsAdjusting()) return;
				updatePlayers();
				updateLabels();
			}
		});
		planetSlider = new JSlider(JSlider.HORIZONTAL,
				MIN_PLANETS, MAX_PLANETS, DEFAULT_PLANETS);
		planetSlider.setAlignmentX(Component.CENTER_ALIGNMENT);
		planetSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(planetSlider.getValueIsAdjusting()) return;
				updateMap();
				updateLabels();
			}
		});
		turnSlider = new JSlider(JSlider.HORIZONTAL,
				MIN_TURNS, MAX_TURNS, DEFAULT_TURNS);
		turnSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				updateLabels();
			}
		});
		
		// Set up buttons
		addButton = new JButton("Add Human Player");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addHumanPlayer();
			}
		});
		okButton = new JButton("OK");
		okButton.addActionListener(this);
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(this);
		defaultsButton = new JButton("Defaults");
		defaultsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reset();
			}
		});
		rejectButton = new JButton("Reject Map");
		rejectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateMap();
			}
		});
		rejectButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JPanel humanPlayerPanel = new JPanel();
		humanPlayerPanel.add(new JLabel("Human player:"));
		humanPlayerPanel.add(playerNameField);
		
		// Top left panel -- label and slider for number of players
		JPanel topLeftPanel = new JPanel();
		topLeftPanel.setLayout(new BoxLayout(topLeftPanel, BoxLayout.PAGE_AXIS));
		topLeftPanel.add(playerLabel);
		topLeftPanel.add(playerSlider);
		topLeftPanel.add(new JLabel("Player list:"));
		topLeftPanel.add(new JScrollPane(playerList));
		topLeftPanel.add(humanPlayerPanel);
		topLeftPanel.add(addButton);
		topLeftPanel.add(Box.createVerticalGlue());
		
		// Top right panel -- lable and slider for number of planets
		JPanel topRightPanel = new JPanel();
		topRightPanel.setLayout(new BoxLayout(topRightPanel, BoxLayout.PAGE_AXIS));
		topRightPanel.add(planetLabel);
		topRightPanel.add(planetSlider);
		topRightPanel.add(new JLabel("Preview map:"));
		topRightPanel.add(new MiniMap());
		topRightPanel.add(rejectButton);
		topRightPanel.add(Box.createVerticalGlue());
		
		// Top panel -- top left and right panels
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.LINE_AXIS));
		topPanel.add(topLeftPanel);
		topPanel.add(topRightPanel);
		
		// Middle panel -- "Turn" slider
		JPanel middlePanel = new JPanel();
		middlePanel.setLayout(new BoxLayout(middlePanel, BoxLayout.PAGE_AXIS));
		middlePanel.add(turnLabel);
		middlePanel.add(turnSlider);
		
		// Button panel -- "OK" and "Cancel" buttons
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.LINE_AXIS));
		bottomPanel.add(defaultsButton);
		bottomPanel.add(Box.createHorizontalGlue());
		bottomPanel.add(okButton);
		bottomPanel.add(cancelButton);
		
		// Main panel -- split and button panels
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.add(topPanel);
		mainPanel.add(middlePanel);
		mainPanel.add(bottomPanel);
		
		// Set up main window
		window = new JDialog(frame, "New Game", true);
		window.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		window.setResizable(false);
		window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		window.add(mainPanel);
		window.pack();
		window.setLocationRelativeTo(frame);
		reset();
	}
	
	/** Shows the dialog and returns whether to start a new game */
	public boolean exec() {
		window.setVisible(true);
		return startGame;
	}
	
	/** Handles button-clicked events */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(okButton)) {
			if(findFirstAIPlayer() == 0) {
				JOptionPane.showMessageDialog(window,
						"The game is much more fun when " +
						"you add a human player!");
				return;
			} else {
				startGame = true;
				setChanged();
				notifyObservers(new Integer(turnSlider.getValue()));
			}
		}
		window.dispose();
	}
	
	/** Updates the parameter labels */
	public void updateLabels() {
		playerLabel.setText("Number of players: " +
				playerSlider.getValue());
		planetLabel.setText("Number of neutral planets: " +
				planetSlider.getValue());
		turnLabel.setText("Number of turns: " +
				turnSlider.getValue());
	}
	
	/** Updates the number of players in the list */
	private void updatePlayers() {
		int diff = playerSlider.getValue() - players.size();
		if(diff == 0) {
			return;
		} else if(diff < 0) {
			for(int i = diff; i < 0; i++) {
				playerListModel.remove(players.size() - 1);
				players.remove(players.size() - 1);
			}
		} else {
			for(int i = 0; i < diff; i++) {
				Player p = Player.createComputerPlayer(players.size());
				playerListModel.addElement(p);
				players.add(p);
			}
		}
		updateMap();
		playerList.repaint();
	}
	
	/** Updates the current map */
	private void updateMap() {
		map.clear();
		map.populate(players, planetSlider.getValue());
		map.emitUpdate();
	}
	
	/** Adds a human player to the top of the player list */
	private void addHumanPlayer() {
		if(playerNameField.getText().equals("")) {
			JOptionPane.showMessageDialog(window,
					"Please enter a name for the player.");
			return;
		}
		int index = findFirstAIPlayer();
		if(index >= MAX_PLAYERS) {
			JOptionPane.showMessageDialog(window,
					"The maximum number of players has " +
					"already been reached.");
		} else {
			Player p = Player.createHumanPlayer(
					playerNameField.getText(), index);
			if(index < players.size()) {
				playerListModel.set(index, p);
				players.set(index, p);
			} else {
				playerListModel.addElement(p);
				players.add(p);
				playerSlider.setValue(index + 1);
			}
			updateMap();
		}
		playerNameField.setText("");
	}
	
	/**
	 * 	Returns the index of the first AI player.  If no AI players
	 * 	exist, the current size of the player list is returned.
	 */
	private int findFirstAIPlayer() {
		int index = 0;
		for(Player p : players) {
			if(p.isAI()) break;
			index++;
		}
		return index;
	}
	
	/** Resets parameters to defaults */
	private void reset() {
		playerSlider.setValue(DEFAULT_PLAYERS);
		planetSlider.setValue(DEFAULT_PLANETS);
		turnSlider.setValue(DEFAULT_TURNS);
		players.clear();
		playerListModel.clear();
		updatePlayers();
		updateLabels();
	}
	
	/** Inner class for mini-map */
	class MiniMap extends JPanel implements Observer {
		
		private Dimension size;
		
		/** Constructor */
		public MiniMap() {
			super();
			size = new Dimension(
					Map.MAP_WIDTH * Sector.MINI_LENGTH,
					Map.MAP_HEIGHT * Sector.MINI_LENGTH);
			setBackground(Color.BLACK);
			setAlignmentX(Component.CENTER_ALIGNMENT);
			map.addObserver(this);
		}
		
		/** Draws the map as a mini-map */
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			map.drawMiniMap(g);
		}
		
		/** Updates the map display */
		public void update(Observable o, Object arg) {
			if(!o.equals(map)) return;
			repaint();
		}
		
		/** Returns the size */
		public Dimension getMinimumSize() {
			return size;
		}
		
		/** Returns the size */
		public Dimension getMaximumSize() {
			return size;
		}
		
		/** Returns the size */
		public Dimension getPreferredSize() {
			return size;
		}
		
	}
	
}
