/**
 * 	Created on Mar 23, 2006
 */

import java.awt.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

/**
 * 	@author Rob Haden
 */
public class MainWindow extends JFrame implements Observer {
	
	public static final int WIDTH = 600;
	public static final int HEIGHT = 640;
	
	private static final String VERSION = "0.9.0";

	private JMenuBar menuBar;
	private JToolBar toolBar;
	private GameBoard gameBoard;
	private GameAction endAction, measureAction, standingsAction, fleetAction;
	
	/** Constructor */
	public MainWindow() {
		// Call super() and set up options
		super("JQuest");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				int choice = JOptionPane.showConfirmDialog(
						MainWindow.this, "Really quit?",
						"Quit JQuest", JOptionPane.OK_CANCEL_OPTION);
				if(choice == JOptionPane.OK_OPTION) {
					System.exit(0);
				}
			}
		});
		
		// Set up game actions
		GameAction newGame = new GameAction("New",
					new ImageIcon("images/New.png"),
					"Start a new game", KeyEvent.VK_N) {
			public void actionPerformed(ActionEvent event) {
				gameBoard.startNewGame();
			}
		};
		GameAction quitGame = new GameAction("Quit",
					null, "Quit the game", KeyEvent.VK_Q) {
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		};
		GameAction aboutGame = new GameAction("About JQuest...",
					null, "Get info about the game",
					KeyEvent.VK_A) {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(MainWindow.this,
						"JQuest\nAuthor: Rob Haden\nVersion: " + VERSION,
						"About JQuest", JOptionPane.INFORMATION_MESSAGE);
			}
		};
		endAction = new GameAction("End",
					new ImageIcon("images/End.png"),
					"End the current game", KeyEvent.VK_E) {
			public void actionPerformed(ActionEvent event) {
				gameBoard.queryGameOver();
			}
		};
		measureAction = new GameAction("Get Distance",
					new ImageIcon("images/Ruler.png"),
					"Get distance", KeyEvent.VK_D) {
			public void actionPerformed(ActionEvent event) {
				gameBoard.measureDistance();
			}
		};
		standingsAction = new GameAction("Help",
					new ImageIcon("images/Help.png"),
					"Open the 'Help' menu", KeyEvent.VK_H) {
			public void actionPerformed(ActionEvent event) {
				gameBoard.showPlayerStats();
			}
		};
		fleetAction = new GameAction("Ships",
					new ImageIcon("images/Ship.png"),
					"Get ship standings", KeyEvent.VK_S) {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "Gets your ship standings",
						"Ship Standings", JOptionPane.INFORMATION_MESSAGE);
			}
		};

		// Set up main game menu
		JMenu gameMenu = new JMenu("Game");
		gameMenu.setMnemonic(KeyEvent.VK_G);
		gameMenu.add(createMenuItem(newGame));
		gameMenu.add(createMenuItem(endAction));
		gameMenu.add(createMenuItem(quitGame));
		
		// Set up help menu
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic(KeyEvent.VK_H);
		helpMenu.add(createMenuItem(aboutGame));
		
		// Set up menu bar
		menuBar = new JMenuBar();
		menuBar.add(gameMenu);
		menuBar.add(helpMenu);
		
		// Set up tool bar and buttons
		toolBar = new JToolBar(SwingConstants.HORIZONTAL);
		toolBar.add(createButton(newGame));
		toolBar.add(createButton(endAction));
		toolBar.addSeparator();
		toolBar.add(createButton(measureAction));
		toolBar.add(createButton(standingsAction));
		toolBar.add(createButton(fleetAction));
		
		// Set up frame and add GUI elements
		gameBoard = new GameBoard();
		add(toolBar, BorderLayout.PAGE_START);
		add(gameBoard, BorderLayout.CENTER);
		setJMenuBar(menuBar);
		pack();
		gameBoard.initGameLogic();
	}
	
	public void update(Observable o, Object arg) {
		if(!(arg instanceof GameLogic.GameState)) return;
		setGameActionsEnabled((GameLogic.GameState)arg);
	}
	
	/** Sets the game action states */
	public void setGameActionsEnabled(GameLogic.GameState state) {
		endAction.setEnabled(state != GameLogic.GameState.NONE);
		measureAction.setEnabled(state == GameLogic.GameState.SOURCE_PLANET);
		standingsAction.setEnabled(state == GameLogic.GameState.SOURCE_PLANET);
		fleetAction.setEnabled(state == GameLogic.GameState.SOURCE_PLANET);
	}
	
	/** Creates a button with the given action */
	private JButton createButton(GameAction action) {
		JButton button = new JButton(action);
		button.setText("");
		return button;
	}
	
	/** Creates a menu item with the given action */
	private JMenuItem createMenuItem(GameAction action) {
		JMenuItem menuItem = new JMenuItem(action);
		menuItem.setIcon(null);
		return menuItem;
	}
	
	/** GameAction inner class */
	abstract class GameAction extends AbstractAction {
		
		/** Constructor */
		protected GameAction(String text, ImageIcon icon,
				String desc, int mnemonic) {
			super(text, icon);
			putValue(SHORT_DESCRIPTION, desc);
			putValue(MNEMONIC_KEY, mnemonic);
		}
		
		/** Reacts to an action event */
		public abstract void actionPerformed(ActionEvent event);
		
	}
	
}
