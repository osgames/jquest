/**
 * 	Created on Mar 23, 2006
 */

import java.awt.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

/**
 * 	@author Rob Haden
 */
public class MapPanel extends JPanel implements Observer {

	private static final int MAP_WIDTH = Map.MAP_WIDTH * Sector.SIDE_LENGTH;
	private static final int MAP_HEIGHT = Map.MAP_HEIGHT * Sector.SIDE_LENGTH;
	
	private Map map;
	
	/** Constructor */
	public MapPanel(Map gameMap) {
		super();
		map = gameMap;
		map.addObserver(this);
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if(e.getX() >= MAP_WIDTH || e.getY() >= MAP_HEIGHT) {
					return;
				}
				map.handleMouseClick(e.getX(), e.getY());
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				if(e.getX() >= MAP_WIDTH || e.getY() >= MAP_HEIGHT) {
					return;
				}
				map.handleMouseMove(e.getX(), e.getY());
			}
		});
		setPreferredSize(new Dimension(
				MAP_WIDTH, MAP_HEIGHT));
		setOpaque(false);
	}
	
	/** Draws the map */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		map.draw(g);
	}
	
	/** Updates the map display */
	public void update(Observable o, Object arg) {
		if(!o.equals(map)) return;
		repaint();
	}
	
}
