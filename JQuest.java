/**
 * 	Created on Mar 17, 2006
 */

import java.awt.Image;

/**
 * 	The JQuest class is the main class for JQuest,
 * 	a Java-based clone of the KDE Linux game Konquest.
 * 	
 * 	@author Rob Haden
 */
public class JQuest {
	
	public static final Image[] PLANET_IMAGES =
		new Image[Planet.NUM_PLANET_TYPES];
	
	public static Image SPLASH_IMAGE;
	
	private MainWindow window;
	private ResourceManager resourceManager;
	
	public static void main(String[] args) {
		new Thread(new Runnable() {
			public void run() {
				new JQuest();
			}
		}).start();
	}
	
	/** Constructor */
	public JQuest() {
		loadResources();
		window = new MainWindow();
		window.setVisible(true);
	}
	
	public void loadResources() {
		resourceManager = new ResourceManager();
		SPLASH_IMAGE = resourceManager.loadImage("konquest-splash.png");
		for(int i = 1; i <= PLANET_IMAGES.length; i++) {
			PLANET_IMAGES[i - 1] = resourceManager.loadImage(
					"planet" + i + ".png");
		}
	}
	
}
