/**
 * 	Created on Mar 18, 2006
 */

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * 	@author Rob Haden
 */
public class Map extends Observable {
	
	public static final int MAP_WIDTH = 16;
	public static final int MAP_HEIGHT = 16;
	
	private Sector[][] sectors = new Sector[MAP_WIDTH][MAP_HEIGHT];
	private int highlightX;
	private int highlightY;
	
	/** Constructor */
	public Map() {
		for(int i = 0; i < MAP_WIDTH; i++) {
			for(int j = 0; j < MAP_HEIGHT; j++) {
				sectors[i][j] = new Sector(i, j);
			}
		}
	}
	
	/** Populates the GameMap with planets */
	public void populate(List<Player> players, int neutralPlanets) {
		// Reset planet names, since we're making a new map
		Planet.resetNameIndex();
		
		// Create a home planet for each player
		for(int i = 0; i < players.size(); i++) {
			findRandomFreeSector().createPlanet(players.get(i));
		}
		
		// Create the specified number of neutral planets
		for(int i = 0; i < neutralPlanets; i++) {
			findRandomFreeSector().createPlanet(null);
		}
	}
	
	/** Clears all of the planets from the map */
	public void clear() {
		for(int i = 0; i < MAP_WIDTH; i++) {
			for(int j = 0; j < MAP_HEIGHT; j++) {
				sectors[i][j].removePlanet();
			}
		}
	}
	
	/** Draws the map */
	public void draw(Graphics g) {
		// Draw all sectors
		for(int i = 0; i < MAP_WIDTH; i++) {
			for(int j = 0; j < MAP_HEIGHT; j++) {
				sectors[i][j].draw(g, i, j);
			}
		}
		
		// Draw the current highlighted sector
		sectors[highlightX][highlightY].drawHighlight(
				g, highlightX, highlightY);
	}
	
	/** Draws the map as a mini-map */
	public void drawMiniMap(Graphics g) {
		for(int i = 0; i < MAP_WIDTH; i++) {
			for(int j = 0; j < MAP_HEIGHT; j++) {
				sectors[i][j].drawMini(g, i, j);
			}
		}
	}
	
	/** Updates the planets on the map */
	public void update() {
		for(int i = 0; i < MAP_WIDTH; i++) {
			for(int j = 0; j < MAP_HEIGHT; j++) {
				sectors[i][j].updatePlanet();
			}
		}
		emitUpdate();
	}
	
	/** Returns a list of the planets owned by the given player */
	public List<Planet> getPlanets() {
		List<Planet> list = new ArrayList<Planet>();
		for(int i = 0; i < MAP_WIDTH; i++) {
			for(int j = 0; j < MAP_HEIGHT; j++) {
				if(sectors[i][j].hasPlanet()) {
					list.add(sectors[i][j].getPlanet());
				}
			}
		}
		return list;
	}
	
	/** Handles mouse clicks forwarded from the display panel */
	public void handleMouseClick(int mouseX, int mouseY) {
		int row = mouseX / Sector.SIDE_LENGTH;
		int column = mouseY / Sector.SIDE_LENGTH;
		// emitUpdate(sectors[row][column]);
		emitUpdate(sectors[row][column].getPlanet());
	}
	
	/** Handles mouse moves forwarded from the display panel */
	public void handleMouseMove(int mouseX, int mouseY) {
		int row = mouseX / Sector.SIDE_LENGTH;
		int column = mouseY / Sector.SIDE_LENGTH;
		highlightX = row;
		highlightY = column;
		emitUpdate(sectors[row][column].getPlanetInfo());
	}
	
	/** Emits an update to the map's observers with an object */
	public void emitUpdate(Object arg) {
		setChanged();
		notifyObservers(arg);
	}
	
	/** Emits an update to the map's observers */
	public void emitUpdate() {
		setChanged();
		notifyObservers();
	}
	
	/** Finds and returns a random free sector */
	private Sector findRandomFreeSector() {
		Sector foundSector = null;
		while(foundSector == null) {
			int x = (int)(Math.random() * MAP_WIDTH);
			int y = (int)(Math.random() * MAP_HEIGHT);
			if(!sectors[x][y].hasPlanet()) {
				foundSector = sectors[x][y];
			}
		}
		return foundSector;
	}
	
}
