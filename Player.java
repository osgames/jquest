/**
 * 	Created on Mar 18, 2006
 */

import java.awt.Color;
import java.util.List;

/**
 * 	@author Rob Haden
 */
public class Player {
	
	private static final Color[] PLAYER_COLORS = {
		new Color(130, 130, 255), Color.YELLOW, Color.RED, Color.GREEN,
		Color.WHITE, Color.CYAN, Color.MAGENTA, new Color(235, 153, 46),
		new Color(106, 157, 104), new Color(131, 153, 128)};
	
	private PlayerAI playerAI;
	private String name;
	
	private int index;
	private int fleetsLaunched;
	private int fleetsDestroyed;
	private int shipsBuilt;
	private int shipsDestroyed;
	private int planetsConquered;
	
	/** Creates a human player */
	public static Player createHumanPlayer(String name, int index) {
		return new Player(name, index, false);
	}
	
	/** Creates a computer player */
	public static Player createComputerPlayer(int index) {
		return new Player("Comp" + (index + 1), index, true);
	}
	
	/** Constructor */
	public Player(String name, int index, boolean isAI) {
		this.name = name;
		this.index = index;
		if(isAI) {
			playerAI = new PlayerAI();
		}
		else {
			playerAI = null;
		}
	}
	
	/** Returns the player's color */
	public Color getColor() {
		return PLAYER_COLORS[index];
	}
	
	/** Returns the player's name */
	public String toString() {
		return name;
	}
	
	/** Returns whether the player is an AI player */
	public boolean isAI() {
		return (playerAI != null);
	}
	
	/** Returns the player's stats as a string array */
	public String[] getStats() {
		return new String[] { toString(),
				String.valueOf(shipsBuilt),
				String.valueOf(planetsConquered),
				String.valueOf(fleetsLaunched),
				String.valueOf(fleetsDestroyed),
				String.valueOf(shipsDestroyed)
		};
	}
	
	/** Increments the number of fleets sent */
	public void updateFleetsSent(int count) {
		fleetsLaunched += count;
	}
	
	/** Increments the number of fleet destroyed */
	public void updateFleetsDestroyed(int count) {
		fleetsDestroyed += count;
	}
	
	/** Increments the number of planets conquered */
	public void updatePlanetsConquered(int count) {
		planetsConquered += count;
	}
	
	/** Increments the number of ships destroyed */
	public void updateShipsDestroyed(int count) {
		shipsDestroyed += count;
	}
	
	/** Increases the number of ships built */
	public void updateShipsBuilt(int count) {
		shipsBuilt += count;
	}
	
	/** Forwards planet search to player AI (if it has one) */
	public Planet searchForPlanet(List<Planet> planets,
								  Planet home, boolean attack) {
		if(!isAI()) return null;
		return playerAI.searchForPlanet(planets, home, attack);
	}
	
	/** Inner class for player AI */
	class PlayerAI {
		
		private final int RANGE = 15;
		
		/**
		 * 	Searches for a planet matching certain criteria in a given list.
		 */
		public Planet searchForPlanet(List<Planet> planets,
									  Planet home, boolean attack) {
			for(Planet target : planets) {
				if(home.getDistance(target) > RANGE || target.equals(home)) {
					continue;
				}
				if(attack && !target.hasOwner(Player.this) &&
						target.getHomeFleet().compareSize(
								home.getHomeFleet().getAttackStrength()) < 0) {
					return target;
				}
				if(!attack && target.hasOwner(Player.this) &&
						target.getHomeFleet().compareSize(
								home.getHomeFleet().getDefenseStrength()) < 0) {
					return target;
				}
			}
			return null;
		}
		
	}
	
}
