/**
 * 	Created on Mar 31, 2006
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

/**
 * 	@author Rob Haden
 */
public class ScoreDialog implements ActionListener {
	
	private static final int[] COLUMN_WIDTHS = {
		54, 81, 136, 119, 122, 118
	};
	
	private JDialog window;
	
	/** Constructor */
	public ScoreDialog(JFrame frame, List<Player> players) {
		// Set up the table and the "OK" button
		JTable table = new JTable(new ScoreTableModel(players));
		JButton okButton = new JButton("OK");
		okButton.addActionListener(this);
		TableColumn column = null;
		for(int i = 0; i < table.getColumnCount(); i++) {
			column = table.getColumnModel().getColumn(i);
			column.setPreferredWidth(COLUMN_WIDTHS[i]);
			column.setResizable(false);
		}
		table.setCellSelectionEnabled(false);
		table.setColumnSelectionAllowed(false);
		table.setShowGrid(false);
		
		// Create the dialog and show it
		window = new JDialog(frame, "Current Standings - JQuest", true);
		window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		window.setLayout(new BoxLayout(
				window.getContentPane(), BoxLayout.Y_AXIS));
		window.add(table.getTableHeader());
		window.add(table);
		window.add(okButton);
		window.pack();
		window.setResizable(false);
		window.setLocationRelativeTo(frame);
		window.setVisible(true);
	}
	
	/** Closes the dialog window when the "OK" button is pressed */
	public void actionPerformed(ActionEvent e) {
		window.dispose();
	}
	
	/** Inner table-model class */
	class ScoreTableModel extends AbstractTableModel {
		
		private String[] columnNames = {
			"Player", "Ships Built", "Planets Conquered",
			"Fleets Launched", "Fleets Destroyed", "Ships Destroyed"
		};
		private int rowCount = 9;
		private List<String[]> playerStats;
		
		/** Constructor */
		public ScoreTableModel(List<Player> players) {
			playerStats = new ArrayList<String[]>();
			for(Player player : players) {
				playerStats.add(player.getStats());
			}
		}
		
		public int getRowCount() {
			return rowCount;
		}
		public int getColumnCount() {
			return columnNames.length;
		}
		public String getColumnName(int column) {
			return columnNames[column];
		}
		public Object getValueAt(int row, int column) {
			if(row >= playerStats.size()) return null;
			return playerStats.get(row)[column];
		}
		
	}
	
}
