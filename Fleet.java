/**
 * 	Created on Mar 18, 2006
 */

import static java.lang.Math.random;
import static java.lang.Math.floor;

/**
 * 	@author Rob Haden
 */
public class Fleet {

	private Planet dest = null;
	private Player owner;
	private int shipCount;
	private int eta = 0;
	private float killPercent;
	
	/** Constructor for home fleet -- no ETA */
	public Fleet(Player owner, int shipCount, float killPercent) {
		this.owner = owner;
		this.shipCount = shipCount;
		this.killPercent = killPercent;
	}
	
	/** Constructor for attack fleet -- has ETA */
	public Fleet(Player owner, int shipCount, float killPercent,
				 Planet dest, int eta) {
		this(owner, shipCount, killPercent);
		this.dest = dest;
		this.eta = eta;
	}
	
	/** Spawns a new fleet from this one */
	public Fleet spawnFleet(int count, Planet dest, int eta) {
		if(count > shipCount) return null;
		removeShips(count);
		owner.updateFleetsSent(1);
		return new Fleet(owner, count, killPercent, dest, eta);
	}
	
	/** Updates the fleet -- decrements its ETA */
	public void update() {
		if(eta > 0) eta--;
	}
	
	/** Returns whether the fleet has arrived at its destination */
	public boolean hasArrived() {
		return (dest != null) && (eta == 0);
	}
	
	/** Adds ships into the fleet */
	public void addShips(int count) {
		shipCount += count;
	}
	
	/** Removes ships from the fleet */
	public void removeShips(int count) {
		shipCount -= count;
	}
	
	/** Rolls to whether the ship attacks */
	public boolean attacks() {
		return (random() < killPercent);
	}
	
	/** Returns whether the fleet is a friendly */
	public boolean isFriendly() {
		return ((dest != null) && dest.hasOwner(owner));
	}
	
	/** Returns whether the fleet is empty (has no ships) */
	public boolean isEmpty() {
		return (shipCount == 0);
	}
	
	/** Merges this fleet into another fleet */
	public void mergeWith(Fleet other) {
		other.addShips(shipCount);
	}
	
	/** Conquers the destination planet */
	public void conquerPlanet() {
		if(dest == null) return;  // Just in case
		dest.conquered(this);
		dest = null;
	}
	
	/** Returns the fleet's owner */
	public Player getOwner() {
		return owner;
	}
	
	/** Returns the fleet's destination planet */
	public Planet getDest() {
		return dest;
	}
	
	/** Returns the number of ships as a string */
	public String getShipCountString() {
		return String.valueOf(shipCount);
	}
	
	/** Returns the fleet's attack strength (for AI players) */
	public int getAttackStrength() {
		return (int)floor(shipCount * 0.7);
	}
	
	/** Returns the fleet's defense strength (for AI players) */
	public int getDefenseStrength() {
		return (int)floor(shipCount * 0.5);
	}
	
	/** Compares the fleet's size to a given integer */
	public int compareSize(int size) {
		return (shipCount - size);
	}
	
	/** Gets the difference in ship count between another fleet */
	public int compareSize(Fleet fleet) {
		return fleet.compareSize(shipCount) * -1;
	}
	
}
