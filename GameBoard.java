/**
 * 	Created on Mar 23, 2006
 */

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

/**
 * 	@author Rob Haden
 */
public class GameBoard extends JPanel implements Observer {
	
	private static final int MESSAGE_WIDTH = 448;
	private static final int MESSAGE_HEIGHT = 33;
	private static final int INFO_WIDTH = 152;
	private static final int LIST_HEIGHT = 116;
	private static final HashMap<GameLogic.GameState, String> uiStates =
		new HashMap<GameLogic.GameState, String>();
	
	static {
		uiStates.put(GameLogic.GameState.SOURCE_PLANET,
				": Select source planet...");
		uiStates.put(GameLogic.GameState.DEST_PLANET,
				": Select destination planet...");
		uiStates.put(GameLogic.GameState.SHIP_COUNT,
				": How many ships?");
		uiStates.put(GameLogic.GameState.RULER_SOURCE,
				"Ruler: Select starting planet...");
		uiStates.put(GameLogic.GameState.RULER_DEST,
				"Ruler: Select ending planet...");
		uiStates.put(GameLogic.GameState.AI_PLAYER,
				": Computer player thinking...");
	}

	private GameLogic logic;
	private JButton endTurnButton;
	private JLabel playerNameLabel, stateMessage, turnCount;
	private JTextArea messageList;
	private JTextField shipCounter;
	private List<Player> players;
	private Map map;
	private PlanetInfo planetInfo;
	private boolean gamePlaying;
	
	/** Constructor */
	public GameBoard() {
		super(new CardLayout());
		super.setBackground(Color.BLACK);
		players = new ArrayList<Player>();
		map = new Map();
		add("splash", createSplashScreen());
		add("game", createGameScreen());
	}
	
	/** Starts up the game logic */
	public void initGameLogic() {
		logic = new GameLogic(players, map);
		logic.addObserver((MainWindow)getParentFrame());
		logic.addObserver(this);
		logic.changeState(GameLogic.GameState.NONE);
		endTurnButton.addActionListener(logic);
		shipCounter.addActionListener(logic);
		shipCounter.addKeyListener(logic);
		addKeyListener(logic);
	}
	
	/** Starts a new game */
	public void startNewGame() {
		queryGameOver();
		if(gamePlaying) return;
		players.clear();
		NewGameDialog newGameDialog = new NewGameDialog(
				getParentFrame(), map, players);
		newGameDialog.addObserver(logic);
		if(!newGameDialog.exec()) return;
		((CardLayout)getLayout()).show(this, "game");
		gamePlaying = true;
	}
	
	/** Queries the user as to whether he wants to end the game */
	public void queryGameOver() {
		if(!gamePlaying) return;
		int choice = JOptionPane.showConfirmDialog(this,
				"Do you wish to retire this game?",
				"End Game", JOptionPane.OK_CANCEL_OPTION);
		if(choice == JOptionPane.OK_OPTION) endGame();
	}
	
	/** Ends the current game */
	public void endGame() {
		if(gamePlaying) {
			showPlayerStats();
			messageList.setText("");
			logic.cleanUpGame();
			gamePlaying = false;
		}
		((CardLayout)getLayout()).show(this, "splash");
	}
	
	/** Measurement toolbar item */
	public void measureDistance() {
		logic.changeState(GameLogic.GameState.RULER_SOURCE);
	}
	
	/** Standings toolbar item */
	public void showPlayerStats() {
		new ScoreDialog(getParentFrame(), players);
	}
	
	/** Updates the game board based on the game state/logic */
	public void update(Observable o, Object arg) {
		if(!(o instanceof GameLogic)) return;
		if(arg instanceof Integer) {
			turnCount.setText("Turn #: " + ((Integer)arg).intValue());
		} else if(arg instanceof String) {
			JOptionPane.showMessageDialog(getParentFrame(), (String)arg,
					"JQuest", JOptionPane.INFORMATION_MESSAGE);
			messageList.append((String)arg + "\n");
			messageList.setCaretPosition(messageList.getDocument().getLength());
		} else if(arg instanceof Player) {
			playerNameLabel.setForeground(((Player)arg).getColor());
			playerNameLabel.setText(((Player)arg).toString());
		} else if(arg instanceof GameLogic.GameState) {
			stateMessage.setText(uiStates.get((GameLogic.GameState)arg));
			shipCounter.setVisible(arg.equals(
					GameLogic.GameState.SHIP_COUNT));
			shipCounter.setText("");
			endTurnButton.setEnabled(arg.equals(
					GameLogic.GameState.SOURCE_PLANET));
			playerNameLabel.setVisible(
					!arg.equals(GameLogic.GameState.RULER_SOURCE) &&
					!arg.equals(GameLogic.GameState.RULER_DEST));
			handleFocus(arg.equals(GameLogic.GameState.SHIP_COUNT));
			if(arg.equals(GameLogic.GameState.NONE)) endGame();
		}
	}
	
	/** Handles the current focus */
	private void handleFocus(boolean value) {
		if(value) {
			shipCounter.requestFocusInWindow();
		} else {
			this.requestFocusInWindow();
		}
	}
	
	/** Creates the game screen */
	private JPanel createGameScreen() {
		JPanel panel = createBoxPanel(BoxLayout.X_AXIS);
		panel.setPreferredSize(new Dimension(
				MainWindow.WIDTH, MainWindow.WIDTH));
		panel.add(createGameStatePanel());
		panel.add(createPlanetInfoPanel());
		return panel;
	}
	
	/** Creates the planet info panel */
	private JPanel createPlanetInfoPanel() {
		planetInfo = new PlanetInfo();
		map.addObserver(planetInfo);
		turnCount = new JLabel();
		turnCount.setHorizontalAlignment(SwingConstants.LEFT);
		turnCount.setForeground(Color.WHITE);
		JPanel panel = createBoxPanel(BoxLayout.Y_AXIS);
		panel.setPreferredSize(new Dimension(
				INFO_WIDTH, MainWindow.WIDTH));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(planetInfo);
		panel.add(Box.createRigidArea(new Dimension(0, 35)));
		panel.add(turnCount);
		panel.add(Box.createVerticalGlue());
		return panel;
	}
	
	/** Creates the game state panel */
	private JPanel createGameStatePanel() {
		messageList = new JTextArea();
		messageList.setForeground(Color.WHITE);
		messageList.setBackground(Color.BLACK);
		messageList.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(messageList,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setPreferredSize(new Dimension(
				MESSAGE_WIDTH, LIST_HEIGHT));
		JPanel panel = createBoxPanel(BoxLayout.Y_AXIS);
		panel.setPreferredSize(new Dimension(
				MESSAGE_WIDTH, MainWindow.WIDTH));
		panel.add(createStateInfoPanel());
		panel.add(new MapPanel(map));
		panel.add(scrollPane);
		return panel;
	}
	
	/** Creates the state info panel */
	private JPanel createStateInfoPanel() {
		playerNameLabel = new JLabel();
		stateMessage = new JLabel();
		stateMessage.setForeground(Color.WHITE);
		shipCounter = new JTextField(4);
		shipCounter.setMaximumSize(new Dimension(40, 20));
		shipCounter.setBackground(Color.GREEN);
		shipCounter.setForeground(Color.WHITE);
		endTurnButton = new JButton("End Turn");
		JPanel panel = createBoxPanel(BoxLayout.X_AXIS);
		panel.setPreferredSize(new Dimension(
				MESSAGE_WIDTH, MESSAGE_HEIGHT));
		panel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 5));
		panel.add(playerNameLabel);
		panel.add(stateMessage);
		panel.add(Box.createHorizontalGlue());
		panel.add(shipCounter);
		panel.add(endTurnButton);
		return panel;
	}
	
	/** Creates a JPanel with a box layout with the given axis */
	private JPanel createBoxPanel(int axis) {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, axis));
		panel.setOpaque(false);
		return panel;
	}
	
	/** Creates the splash screen */
	private JPanel createSplashScreen() {
		JPanel panel = new JPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(JQuest.SPLASH_IMAGE, 0, 0, null);
			}
		};
		panel.setOpaque(false);
		return panel;
	}
	
	/** Returns the parent frame */
	private JFrame getParentFrame() {
		Container c = this;
		while(!(c instanceof JFrame)) {
			c = c.getParent();
		}
		return (JFrame)c;
	}
	
}
